(org-babel-load-file
 (expand-file-name "initial.org"
                   user-emacs-directory))


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "red3" "ForestGreen" "yellow3" "blue" "magenta3" "DeepSkyBlue" "gray50"])
 '(awesome-tray-mode-line-active-color "#29aeff")
 '(awesome-tray-mode-line-inactive-color "#2f2f2f")
 '(custom-safe-themes
   '("1cff217fb3814216d322f0b8b1adcdb913d6f5cfa6a781cff13f84ce9cad36a1" "1fd225536c89f564410320789562e5ef95fcf27003e0d163ac4afab96e4c8915" default))
 '(ebrowse--indentation 6)
 '(ebrowse-source-file-column 90)
 '(flymake-error-bitmap '(flymake-double-exclamation-mark modus-theme-fringe-red))
 '(flymake-fringe-indicator-position nil)
 '(flymake-note-bitmap '(exclamation-mark modus-theme-fringe-cyan))
 '(flymake-warning-bitmap '(exclamation-mark modus-theme-fringe-yellow))
 '(grep-command "grep --recursive --color -nH --null -e")
 '(grep-files-aliases
   '(("all" . "* .[!.]* ..?*")
     ("el" . "*.el")
     ("ch" . "*.[ch]")
     ("c" . "*.c")
     ("cc" . "*.cc *.cxx *.cpp *.C *.CC *.c++")
     ("cchh" . "*.cc *.[ch]xx *.[ch]pp *.[CHh] *.CC *.HH *.[ch]++")
     ("hh" . "*.hxx *.hpp *.[Hh] *.HH *.h++")
     ("h" . "*.h")
     ("l" . "[Cc]hange[Ll]og*")
     ("m" . "[Mm]akefile*")
     ("tex" . "*.tex")
     ("texi" . "*.texi")
     ("asm" . "*.[sS]")
     ("lisp" . "*.lisp")))
 '(grep-find-abbreviate t)
 '(grep-find-command
   '("find . -type f -exec grep --color -nH --null -e  \\{\\} +" . 49))
 '(grep-template "grep --recursive <X> <C> -nH --null -e <R> <F>")
 '(helm-minibuffer-history-key "M-p")
 '(highlight-tail-colors '(("#2f4a00" . 0) ("#00415e" . 20)))
 '(hl-todo-keyword-faces
   '(("HOLD" . "#cfdf30")
     ("TODO" . "#feacd0")
     ("NEXT" . "#b6a0ff")
     ("THEM" . "#f78fe7")
     ("PROG" . "#00d3d0")
     ("OKAY" . "#4ae8fc")
     ("DONT" . "#80d200")
     ("FAIL" . "#ff8059")
     ("DONE" . "#44bc44")
     ("NOTE" . "#f0ce43")
     ("KLUDGE" . "#eecc00")
     ("HACK" . "#eecc00")
     ("TEMP" . "#ffcccc")
     ("FIXME" . "#ff9977")
     ("XXX+" . "#f4923b")
     ("REVIEW" . "#6ae4b9")
     ("DEPRECATED" . "#aaeeee")))
 '(hyperbole-web-search-alist
   '(("Dictionary" . "https://en.wiktionary.org/wiki/%s")
     ("Hub(git)" . "https://github.com/search?ref=simplesearch&q=%s")
     ("RFCs" . "https://tools.ietf.org/html/rfc%s")
     ("StackOverflow" . "https://stackoverflow.com/search?q=%s")
     ("Wikipedia" . "https://en.wikipedia.org/wiki/%s")))
 '(hyperbole-web-search-browser-function t)
 '(ibuffer-deletion-face 'dired-flagged)
 '(ibuffer-filter-group-name-face 'dired-mark)
 '(ibuffer-marked-face 'dired-marked)
 '(ibuffer-title-face 'dired-header)
 '(inhibit-startup-screen t)
 '(org-agenda-files '("~/org/home.org" "~/org/uni.org"))
 '(package-selected-packages '(vterm modus-vivendi-theme evil exwm use-package))
 '(vc-annotate-background nil)
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   '((20 . "#ff8059")
     (40 . "#feacd0")
     (60 . "#f78fe7")
     (80 . "#f4923b")
     (100 . "#eecc00")
     (120 . "#cfdf30")
     (140 . "#f8dec0")
     (160 . "#bfebe0")
     (180 . "#44bc44")
     (200 . "#80d200")
     (220 . "#6ae4b9")
     (240 . "#4ae8fc")
     (260 . "#00d3d0")
     (280 . "#c6eaff")
     (300 . "#29aeff")
     (320 . "#72a4ff")
     (340 . "#00bdfa")
     (360 . "#b6a0ff")))
 '(vc-annotate-very-old-color nil)
 '(warning-suppress-log-types '((bytecomp)))
 '(xterm-color-names
   ["#000000" "#ff8059" "#44bc44" "#eecc00" "#29aeff" "#feacd0" "#00d3d0" "#a8a8a8"])
 '(xterm-color-names-bright
   ["#181a20" "#f4923b" "#80d200" "#cfdf30" "#72a4ff" "#f78fe7" "#4ae8fc" "#ffffff"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ebrowse-member-class ((t (:slant italic))))
 '(ebrowse-root-class ((t (:background "black" :underline "light gray" :weight bold))))
 '(semantic-highlight-func-current-tag-face ((t (:box (:line-width 2 :color "dark red" :style released-button)))))
 '(tab-bar ((t nil)))
 '(tab-bar-tab ((t (:background "black" :inverse-video t :box (:line-width 2 :color "white" :style released-button)))))
 '(tab-bar-tab-inactive ((t nil))))
