;; Empty to avoid analyzing files when loading remote files.

    ;; Emacs configuration file content is written below.
;;(setq gc-cons-threshold (* 50 1000 1000))
(setq gc-cons-threshold 100000000)
(let (;; temporarily increase `gc-cons-threshold' when loading to speed up startup.
      ;; Empty to avoid analyzing files when loading remote files.
      (file-name-handler-alist nil))

    ;; Emacs configuration file content is written below.

(let ((default-directory (expand-file-name "lisp" user-emacs-directory)))
  (normal-top-level-add-subdirs-to-load-path))

(defun fer/compile-all ()
  (mapc (lambda (directory)
	  (byte-recompile-directory directory 0 nil))
	load-path))



(require 'use-package)
(use-package use-package
  :config 
  (setq use-package-always-ensure nil
	use-package-always-defer nil
	use-package-always-demand nil
	use-package-expand-minimally nil
	use-package-enable-imenu-support t
	use-package-hook-name-suffix nil))

(setq auto-save-default nil
      create-lockfiles nil
      delete-old-versions -1
      vc-make-backup-files t)

(setq initial-scratch-message "")

(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(tool-bar-mode -1)


(when (and window-system (font-info "JuliaMono"))
  (set-face-attribute 'default nil :font "JuliaMono" :height 130 ))


(toggle-scroll-bar -1)
(column-number-mode 1)

(setq resize-mini-windows t
      max-mini-window-height 0.33)

(use-package undo-tree
  :init
  (setq undo-tree-visualizer-diff t)
  :config
  (progn
    (setq undo-tree-visualizer-timestamps t)
    (global-undo-tree-mode)))


(use-package startup
  :defer t
  :config (setq inhibit-startup-screen t))

(use-package ediff
  :defer t
  :ensure nil
  :config
  (setq ediff-window-setup-function #'ediff-setup-windows-plain
	ediff-split-window-function #'split-window-horizontally))

;; Restore window configuration after ediff ends.
(add-hook 'ediff-load-hook
	  (lambda ()
	    (add-hook 'ediff-before-setup-hook
		      (lambda ()
			(setq ediff-saved-window-configuration (current-window-configuration))))
	    (let ((restore-window-configuration
		   (lambda ()
		     (set-window-configuration ediff-saved-window-configuration))))
	      (add-hook 'ediff-quit-hook restore-window-configuration 'append)
	      (add-hook 'ediff-suspend-hook restore-window-configuration 'append))))

(defun fer/command-line-diff (switch)
  "Add -diff option to Emacs command line, SWITCH."
  (let ((file1 (pop command-line-args-left))
	(file2 (pop command-line-args-left)))
    (ediff file1 file2)))

(add-to-list 'command-switch-alist '("diff" . fer/command-line-diff))

(setq warning-minimum-level :emergency)

(use-package exec-path-from-shell
  :defer t
  :config (exec-path-from-shell-initialize))

(use-package dired-async
  :after dired
  :config (dired-async-mode 1))

(use-package async)

;; (use-package crux
;;   :defer t
;;   :straight t)

(use-package smartparens
  :defer 1
  :config (smartparens-global-mode))

(use-package smooth-scrolling
  :config
  (smooth-scrolling-mode 1))


(use-package  no-littering
  :defer t
  :config (progn
	    (add-to-list 'recentf-exclude no-littering-var-directory)
	    (add-to-list 'recentf-exclude no-littering-etc-directory)
	    (setq auto-save-file-name-transforms
		  `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))
	    (setq custom-file (no-littering-expand-etc-file-name "custom.el"))))

(use-package modus-vivendi-theme)

(load-theme 'modus-vivendi t)

(use-package mood-line
  :config (mood-line-mode))


(use-package all-the-icons)

(use-package page-break-lines)

(use-package dashboard
  :config
  (progn (setq
	  dashboard-startup-banner "~/.emacs.d/image/che-stallman.png"
	  dashboard-banner-logo-title nil
	  dashboard-set-navigator t
	  dashboard-set-heading-icons t
	  dashboard-set-file-icons t
	  dashboard-center-content t
	  dashboard-set-heading-icons t
	  dashboard-items '((recents  . 10))
	  dashboard-navigator-buttons
	  `((("" "Gitlab" "Show stars" (lambda (&rest _) (browse-url-firefox "https://gitlab.com/sasanidas")))
	     ("" "Agenda" "Show stars" (lambda (&rest _) (org-agenda))))))
	 (dashboard-setup-startup-hook)))

(with-eval-after-load 'evil-maps
  (add-to-list 'evil-emacs-state-modes 'dashboard-mode)
  (add-to-list 'evil-emacs-state-modes 'vterm-mode)
  (add-to-list 'evil-emacs-state-modes 'helpful-mode)
  (add-to-list 'evil-emacs-state-modes 'bufler-list-mode)
  (add-to-list 'evil-emacs-state-modes 'flycheck-error-list-mode)
  (add-to-list 'evil-emacs-state-modes 'eww-mode)
  (add-to-list 'evil-emacs-state-modes 'cider-stacktrace-mode)
  (add-to-list 'evil-emacs-state-modes 'xref--xref-buffer-mode)
  (define-key evil-motion-state-map (kbd "/") 'helm-swoop))



(use-package evil
  :config (evil-mode)
  (setq evil-want-keybinding nil))

(use-package annalist)

(use-package evil-collection
  :after (evil annalist)
  :config
  (evil-collection-init))

(use-package ace-window
  :defer t
  :config
    (setq aw-background nil)
    (hkey-ace-window-setup))
    (global-set-key (kbd "C-c a") 'ace-window)

(use-package zoom-window
  :defer t
  :bind (("C-c C-j" . zoom-window-zoom))
  :config
  (setq zoom-window-mode-line-color "brown"))



(use-package helm
  :config
  (progn
    (setq helm-candidate-number-limit 100)
    ;; From https://gist.github.com/antifuchs/9238468
    (setq helm-idle-delay 0.0 
	  helm-input-idle-delay 0.01
	  helm-yas-display-key-on-candidate t
	  helm-display-header-line nil
	  helm-mode-line-string ""
	  helm-quick-update t
	  helm-M-x-requires-pattern nil
	  helm-ff-skip-boring-files t)

    (use-package helm-command
      :config
      (global-set-key (kbd "C-SPC") 'helm-M-x))

    (use-package helm-find-files)
    (global-set-key (kbd "C-x C-f") 'helm-find-files)

    (use-package helm-for-files
      :config
      (global-set-key (kbd "C-x b") 'helm-for-files))

    (use-package helm-buffers
      :config
      (global-set-key (kbd "C-S-SPC") 'helm-buffers-list))

    (use-package helm-elisp
      :config
      (global-set-key (kbd "C-h f") 'helm-apropos)
      (global-set-key (kbd "C-h C-l") 'helm-locate-library))

    (use-package helm-info
      :config
      (global-set-key (kbd "C-h r") 'helm-info-emacs))

    (use-package helm-ring
      :config
      (global-set-key (kbd "C-c k") 'helm-show-kill-ring)
      (global-set-key (kbd "C-c m") 'helm-all-mark-rings))))
(use-package helm-mode

  :config
  (helm-mode))

;; (use-package helm-fd
;; 	:straight (helm-fd :fetcher git :url "https://gitlab.com/sasanidas/helm-fd")
;; 	:bind (("C-c f" . helm-fd)))

(use-package helm-swoop 
  :after helm)

(add-to-list 'display-buffer-alist
	     `(,(rx bos "*helm" (* not-newline) "*" eos)
	       (display-buffer-in-side-window)
	       (inhibit-same-window . t)
	       (window-height . 0.4)))

(use-package dired
  :defer t
  :ensure nil
  :config
  (setq delete-by-moving-to-trash t
	dired-listing-switches "-alh"))
(eval-after-load "dired"
  #'(lambda ()
      (put 'dired-find-alternate-file 'disabled nil)
      (define-key dired-mode-map (kbd "RET") #'dired-find-alternate-file)))

(defun fer/dired-do-ispell (&optional arg)
  (interactive "P")
  (dolist (file (dired-get-marked-files
		 nil arg
		 #'(lambda (f)
		     (not (file-directory-p f)))))
    (save-window-excursion
      (with-current-buffer (find-file file)
	(ispell-buffer)))
    (message nil)))

(defun fer/dired-ediff-files ()
  (interactive)
  (let ((files (dired-get-marked-files))
	(wnd (current-window-configuration)))
    (if (<= (length files) 2)
	(let ((file1 (car files))
	      (file2 (if (cdr files)
			 (cadr files)
		       (read-file-name
			"file: "
			(dired-dwim-target-directory)))))
	  (if (file-newer-than-file-p file1 file2)
	      (ediff-files file2 file1)
	    (ediff-files file1 file2))
	  (add-hook 'ediff-after-quit-hook-internal
		    (lambda ()
		      (setq ediff-after-quit-hook-internal nil)
		      (set-window-configuration wnd))))
      (error "No more than 2 files should be marked"))))

(use-package dired-x
  :defer t
  :ensure nil
  :after dired)

(global-set-key (kbd "C-c d") 'projectile-dired)

(use-package dired-k
  :after dired
  :defer t
  :config (setq dired-k-padding 2
		dired-k-human-readable t))

(use-package dired-git-info
  :after dired
  :config (setq dgi-auto-hide-details-p nil
		dgi-commit-message-format "%s     %cr     %cn     %ce"))

;; (add-hook 'dired-after-readin-hook 'dired-git-info-auto-enable)

(use-package all-the-icons-dired
:after all-the-icons)

(add-hook 'dired-after-readin-hook 'all-the-icons-dired-mode)

(with-eval-after-load "major-mode-hydra"
  (major-mode-hydra-define dired-mode
    (:title "Dired Mode" :color pink :separator "=" :exit t)
    ("File"
     (
      ("z" dired-create-empty-file "create")
      ("G" dired-do-chgrp "chgrp")
      ("M" dired-do-chmod "chmod")
      ("m" dired-mark "mark")
      ("O" dired-display-file "display")
      ("o" dired-find-file-other-window "display other window")
      ("R" dired-do-rename "rename")
      ("S" dired-do-symlink "symlink")
      ("v" dired-view-file "view")      ;; q to exit, s to search, = gets line #
      ("Z" dired-do-compress "compress"))
     "Files"
     (
					;("F" dired-do-find-marked-files "open-files")
      ("C" dired-do-copy "copy-files")        ;; Copy all marked files
      ("D" dired-do-delete "delete-files")
      ("P" dired-omit-mode "omit files"))
     "Mark"
     (("t" dired-toggle-marks "toogle")
      ("U" dired-unmark-all-marks "unmark all")
      ("u" dired-unmark "unmark")
      ("E" dired-mark-extension "mark-extensions")
      ("+" dired-create-directory "create directory")
      ("?" dired-summary "summary"))
     "Diff"
     (("e" fer/dired-ediff-files "ediff-files"))
     "Misc"
     (("f" dired-goto-file "go to file")
      ("V" dired-k "show git info")
      ("I" dired-git-info-mode "commit git info")
      ("g" revert-buffer "revert buffer")
      ("l" dired-do-redisplay "redisplay")   ;; relist the marked or singel directory
      ("h" dired-hide-details-mode "hide details")
      ("s" dired-sort-toggle-or-edit "toogle-date")
      ("i" fer/dired-do-ispell "ispell")
      ("A" dired-do-find-regexp "find-regex")
      ("q" nil "quit")))))

(use-package perspeen
  :config
  (progn
    (setq perspeen-workspace-default-name "Main")
    (perspeen-mode)))

(with-eval-after-load "hydra"
    (progn
      (defhydra hydra-perspeen (:columns 1 :exit t)
	"
Perspeen^^^^                  
----------------"
	("c"  perspeen-create-ws "Create WS")
	("n"  perspeen-next-ws "Create WS")
	("h"  helm-perspeen "Helm perspeen")
	("d"  perspeen-delete-ws "Delete current WS")
	("q" nil "Quit"))
      (global-set-key (kbd "C-c p") 'hydra-perspeen/body)))

(use-package helm-perspeen
  :after helm)

(use-package hydra)

(use-package major-mode-hydra
  :after hydra
  :bind (("C-c h" . major-mode-hydra)))

(use-package helpful
  :defer t
  :config
  :bind (("C-h f" . helpful-callable)
	 ("C-h v" . helpful-variable)
	 ("C-h k" . helpful-key)
	 ("C-c C-d" . helpful-at-point)))

(use-package org
  :defer t
  :bind (:map  org-mode-map
	       ("C-c C-j" . nil)))


  ;; (let* ((html-stye (with-current-buffer
  ;; 			  (find-file-noselect (format "%s/var/org/style.html" user-emacs-directory))
  ;; 			(buffer-substring-no-properties (point-min) (point-max)))))
  ;;   (setq org-html-head html-stye))

(use-package ox-gfm
  :after org)

(use-package org-agenda
  :defer t
  :config
  (setq org-agenda-files (list "~/org/home.org"
			       "~/org/uni.org")))

(defun fer/org-gfm-src-block (src-block contents info)
  "Transcode SRC-BLOCK element into Gitlab Flavored Markdown
		format. CONTENTS is nil.  INFO is a plist used as a communication
		channel."
  (let* ((lang (org-element-property :language src-block))
	 (code (org-export-format-code-default src-block info))
	 (prefix (concat "```"
			 (if (string-equal "emacs-lisp" lang)
			     "lisp"
			   lang)
			 "\n"))
	 (suffix "```"))
    (concat prefix code suffix)))

(defun fer/org-gfm-export-to-markdown (&optional async subtreep visible-only)
  (interactive)
  (let ((outfile (concat (format-time-string "%Y-%m-%e") "-" (f-base (buffer-file-name)) ".md")))
    (org-export-to-file 'gfm outfile async subtreep visible-only)
    (with-temp-file outfile
      (let* ((title (read-from-minibuffer "Inserta titulo: "))
	     (header (concat   "+++
  title = \"" title "\"
  description = \"Article\"
  +++\n\n")))
	(insert header)
	(insert-file-contents outfile)))
    (rename-file outfile (concat (projectile-project-root) "content" (f-path-separator) outfile) t)))

(advice-add 'org-gfm-src-block :override #'fer/org-gfm-src-block)
(advice-add 'org-gfm-export-to-markdown :override #'fer/org-gfm-export-to-markdown)

(defalias 'fer/org-finish-article 'org-gfm-export-to-markdown)

(setq async-shell-command-display-buffer nil)

 (use-package vterm
   :bind (:map vterm-mode-map
	       ("C-<SPC>" . nil)
	       ("C-c u" . nil)))


 (use-package vterm-toggle
   :after vterm
   :config
   (progn
     (setq vterm-toggle-fullscreen-p nil)
     (setq vterm-toggle-scope 'project)
     (add-to-list 'display-buffer-alist
		  '((lambda(bufname _) (with-current-buffer bufname (equal major-mode 'vterm-mode)))
		    (display-buffer-reuse-window display-buffer-at-bottom)
		    (direction . bottom)
		    (dedicated . t) ;dedicated is supported in emacs27
		    (reusable-frames . visible)
		    (window-height . 0.3)))))

(global-set-key (kbd "C-c u") #'vterm-toggle)

(add-hook 'vterm-toggle-show-hook (lambda ()
				    (global-hl-line-mode -1)))

(add-hook 'vterm-toggle-hide-hook (lambda ()
				    (global-hl-line-mode)))

(use-package hyperbole)

(defun fer/generate-tags (mode &optional extensions)
  "Generate TAG file in the root of the project.
	  It requires MODE as a mode name, and EXTENSIONS optionally."
  (interactive)
  (let* ((project-root (projectile-project-root))
	 (mode-extension nil)
	 (language nil))
    (cond ((equal mode 'c-mode)
	   (setq mode-extension ".h.c"
		 language "c"))
	  ((equal mode 'c++-mode)
	   (setq mode-extension ".h.c.hpp.cpp"
		 language "c++"))
	  ((equal mode 'lisp-mode)
	   (setq mode-extension ".lisp.clisp"
		 language "lisp"))
	  ((equal mode 'php-mode)
	   (setq mode-extension ".php.inc.module"
		 language "php")))

    (if (and (not mode-extension) (not extensions))
	(error "Error in the mode detection")
      (async-shell-command
       (format "cd %s && ctags-universal -h \"%s\"  --languages=%s --append=yes -eR %s"
	       (shell-quote-argument project-root)
	       mode-extension
	       language
	       (shell-quote-argument project-root))))))


;; Source https://github.com/ocodo/.emacs.d/
(defun fer/switch-to-scratch ()
  "Switch to scratch, grab the region if it's active."
  (interactive)
  (let ((contents
	 (and (region-active-p)
	      (buffer-substring (region-beginning)
				(region-end)))))
    (switch-to-buffer "*scratch*")
    (if contents
	(progn
	  (goto-char (buffer-end 1))
	  (insert contents)))))

(defun fer/describe-thing-at-point ()
  "Describre thing at point."
  (interactive)
  (let* ((thing (symbol-at-point)))
    (cond
     ((fboundp thing) (describe-function thing))
     ((boundp thing) (describe-variable thing)))))

(defun fer/diff-region ()
  "Select a region to compare."
  (interactive)
  (when (use-region-p)  ; there is a region
    (let (buf)
      (setq buf (get-buffer-create "*Diff-regionA*"))
      (save-current-buffer
	(set-buffer buf)
	(erase-buffer))
      (append-to-buffer buf (region-beginning) (region-end))))
  (message "Now select other region to compare and run `diff-region-now`"))

(defun fer/diff-region-now ()
  "Compare current region with region already selected by `diff-region`."
  (interactive)
  (when (use-region-p)
    (let (bufa bufb)
      (setq bufa (get-buffer-create "*Diff-regionA*"))
      (setq bufb (get-buffer-create "*Diff-regionB*"))
      (save-current-buffer
	(set-buffer bufb)
	(erase-buffer))
      (append-to-buffer bufb (region-beginning) (region-end))
      (ediff-buffers bufa bufb))))

(defun fer/make-yas-from-region (begin end)
  "Make a yasnippet from the current region BEGIN END.
	  You should use standard snippet formatting in place, e.g. $1,
	  ${1:default value} and so on.  See the yasnippet docs for more info.
	  You'll be prompted for a name, trigger key and when `prefix-arg' is
	  specified, a snippet group."
  (interactive "r")
  (if (region-active-p)
      (progn
	;; TODO make a new buffer with yas headers
	;; ask for a name
	(let* ((name (read-from-minibuffer "Name: "))
	       (group (if current-prefix-arg
			  (format "\n# group: %s\n" (read-from-minibuffer "Group: "))
			""))
	       (key (read-from-minibuffer "Key: "))
	       (filename (format "%ssnippets/%s/%s" user-emacs-directory major-mode name))
	       (snippet (buffer-substring begin end))
	       (template (format "# -*- mode: snippet -*-
	  # name: %s%s
	  # key: %s
	  # --
	  %s
	  "
				 name
				 group
				 key
				 snippet)))
	  (with-temp-buffer
	    (insert template)
	    (write-file filename)))
	(yas-reload-all))
    (error "An active region is needed to make a snippet")))


(use-package ov)

(with-eval-after-load 'magit
  (defun fer/magit-log--add-date-headers (&rest _ignore)
    "Add date headers to Magit log buffers."
    (when (derived-mode-p 'magit-log-mode)
      (save-excursion
	(ov-clear 'date-header t)
	(goto-char (point-min))
	(cl-loop with last-age
		 for this-age = (-some--> (ov-in 'before-string 'any (line-beginning-position) (line-end-position))
				  car
				  (overlay-get it 'before-string)
				  (get-text-property 0 'display it)
				  cadr
				  (s-match (rx (group (1+ digit) ; number
						      " "
						      (1+ (not blank))) ; unit
					       (1+ blank) eos)
					   it)
				  cadr)
		 do (when (and this-age
			       (not (equal this-age last-age)))
		      (ov (line-beginning-position) (line-beginning-position)
			  'after-string (propertize (concat " " this-age "\n")
						    'face 'magit-section-heading)
			  'date-header t)
		      (setq last-age this-age))
		 do (forward-line 1)
		 until (eobp)))))

  (define-minor-mode fer/magit-log-date-headers-mode
    "Display date/time headers in `magit-log' buffers."
    :global t
    (if fer/magit-log-date-headers-mode
	(progn
	  ;; Enable mode
	  (add-hook 'magit-post-refresh-hook #'fer/magit-log--add-date-headers)
	  (advice-add #'magit-setup-buffer-internal :after #'fer/magit-log--add-date-headers))
      ;; Disable mode
      (remove-hook 'magit-post-refresh-hook #'fer/magit-log--add-date-headers)
      (advice-remove #'magit-setup-buffer-internal #'fer/magit-log--add-date-headers)))

  (fer/magit-log-date-headers-mode))


;; https://www.apertium.org/apy/translate?q=buenas%20tardes&langpair=spa|eng
(require 'json)
(require 'dom)

(defun fer/ap-transalte ()
  "Translate active region english to spanish."
  (interactive)
  (let* ((region (buffer-substring-no-properties (region-beginning)
						 (region-end)))
	 (ap-base "https://www.apertium.org/apy/translate")
	 (ap-parameter "&langpair=eng|spa")
	 (response nil))
    (with-current-buffer (url-retrieve-synchronously
			  (format "%s?%s%s" ap-base (url-build-query-string `(("q"  ,region)))
				  ap-parameter))
      (setq response (json-parse-string (buffer-substring-no-properties
					 url-http-end-of-headers
					 (point-max))))
      (message "%s"
	       (gethash "translatedText" (gethash "responseData" response))))))



(cl-defun fer/external-launcher (&key software-name )
  "Launch external application within Emacs."
  (interactive)
  (cond ((string-equal software-name "pcmanfm")
	 (shell-command (format "pcmanfm %s" default-directory)))))


;; (defhydra hydra-launcher (:columns 1 :exit t)
;;   "
;;  ---------------- Launcher^^^^----------------"
;;   ("p"  (fer/external-launcher :software-name "pcmanfm")  "Pcmanfm")
;;   ("q" nil "Quit"))

;; (global-set-key (kbd "C-c C-p") 'hydra-launcher/body)

(use-package helm-tramp
  :defer t)

(add-hook 'helm-tramp-pre-command-hook '(lambda () (global-aggressive-indent-mode 0)
					  (projectile-mode 0)))

(add-hook 'helm-tramp-quit-hook '(lambda () (global-aggressive-indent-mode 1)
				   (projectile-mode 1)))

(define-key global-map (kbd "C-c s") 'helm-tramp)

(use-package zone
  :defer t
  :config (zone-when-idle 180))

(which-function-mode)

(global-hl-line-mode)

(use-package flycheck
  :config (global-flycheck-mode))

(use-package flyspell
  :after flycheck
  :init (add-hook 'prog-mode-hook #'flyspell-prog-mode))

(use-package evil-collection-magit
  :config (evil-collection-magit-init))

(use-package magit
  :config
  (progn
    (global-set-key (kbd "C-x g") 'magit-status)
    (setq evil-magit-state 'motion)))


(use-package magit-todos
  :defer t
  :after magit)

(use-package magit-gitflow
  :after magit)

(use-package diff-hl
  :config (global-diff-hl-mode))

(add-hook 'magit-mode-hook 'turn-on-magit-gitflow)

(use-package projectile
  :config (projectile-global-mode))

(use-package projectile-speedbar)

(use-package speedbar
  :bind (:map  speedbar-mode-map
	       ("q" . sr-speedbar-close)))

(use-package sr-speedbar
  :after speedbar
  :config
  (setq sr-speedbar-right-side nil))

(global-set-key (kbd "C-c o") 'sr-speedbar-toggle)

(defun fer/speedbar-expand-tags ()
  "Expand current `sr-speedbar' buffer file."
  (interactive)
  ;; We assume that the speedbar name is the same as the file of the buffer
  (let* ((current-buffer-name (file-name-nondirectory (buffer-file-name)))
	 (file-point nil)
	 (line-list '()))
    (with-current-buffer speedbar-buffer
      ;; Refresh the current speedbar buffer
      (speedbar-refresh)
      (goto-char (point-min))
      (re-search-forward current-buffer-name)
      (setq file-point (point))
      ;; This function make the point go backwards so we have to save the location
      (speedbar-flush-expand-line)
      (goto-char file-point)
      ;; We enter the "expanded" attributes
      (forward-line)
      (while
	  ;; Check if we reach another file, or the end of the buffer.
	  (and (not (speedbar-line-file))
	       (not (equal (point) (point-max))))
	(push (point) line-list)
	(forward-line))
      ;; Once we have the point of the main branches, we iterate
      ;; and expand his content
      (seq-map (lambda (line)
		 (goto-char line)
		 (speedbar-flush-expand-line))
	       line-list))))

(use-package skeletor
  :config
  (setq skeletor-project-directory (expand-file-name "Programming" (getenv "HOME"))
	skeletor-user-directory (locate-user-emacs-file (expand-file-name "skeletor" "var" ))))

(defun fer/init-iup-project (path)
  "Initial command for a IUP project."
  (skeletor-async-shell-command "chmod +x get-libs.sh && ./get-libs.sh"))

(defun fer/flycheck-cpp-path (path)
  (let* ((include-path (concat path (f-path-separator) "include/"))
	 (local-dir (concat path (f-path-separator)".dir-locals.el")))
    (with-temp-file local-dir
      (insert
       (format "((nil . ((flycheck-clang-include-path . (\"%s\") ))))" include-path)))))


(skeletor-define-template "Cpp-template"
  :title "C++ project" :default-license (rx bol "gpl")
  :after-creation fer/flycheck-cpp-path)

(skeletor-define-template "C-IUP-template"
  :title "C IUP project" :default-license (rx bol "gpl")
  :after-creation fer/init-iup-project)

(skeletor-define-template "Emacs-lisp-template"
  :title "Emacs lisp project" :default-license (rx bol "gpl"))

(skeletor-define-template "Common-lisp-template"
  :title "Common lisp project" :default-license (rx bol "gpl"))

(use-package company
  :config
  (progn
    (setq
     company-idle-delay 0.3
     company-tooltip-limit 24            
     commentmpany-idle-delay .2              
     company-echo-delay 0              
     company-minimum-prefix-length 2
     company-require-match nil
     company-dabbrev-ignore-case nil
     company-tooltip-align-annotations t 
     company-dabbrev-downcase nil)
    (add-to-list 'company-backends '(company-capf :with company-dabbrev))
    (global-company-mode)))

(define-key company-active-map (kbd "C-n") 'company-select-next)
(define-key company-active-map (kbd "C-p") 'company-select-previous)

(use-package company-try-hard
  :after company)

(global-set-key (kbd "C-c z") #'company-try-hard)
(define-key company-active-map (kbd "C-c z") #'company-try-hard)

(use-package simple-tree
  :init
  (progn
    (global-set-key "\C-x\C-l" 'simple-tree-toggle)
    (global-set-key "\C-x\C-w" 'simple-tree-project-remove)
    (global-set-key "\C-x\C-y" 'simple-tree-project-add)))

(use-package rg)

(use-package helm-projectile
  :after helm)

(setq tags-revert-without-query t)
;; Do case-sensitive tag searches
(setq tags-case-fold-search nil) ;; t=case-insensitive, nil=case-sensitive
;; Don't warn when TAGS files are large
(setq large-file-warning-threshold nil)

(use-package realgud
  :defer t)

(use-package erefactor)

(add-hook 'emacs-lisp-mode-hook 'erefactor-lazy-highlight-turn-on)
(add-hook 'lisp-interaction-mode-hook 'erefactor-lazy-highlight-turn-on)

(use-package nameless)

(add-hook 'emacs-lisp-mode-hook #'nameless-mode)

(use-package flycheck-package
  :after flycheck)

(with-eval-after-load 'flycheck
  '(flycheck-package-setup))

(use-package flycheck-relint)

(with-eval-after-load 'flycheck-relint
  (flycheck-relint-setup))



(use-package aggressive-indent)

(add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)

(with-eval-after-load 'major-mode-hydra
  (major-mode-hydra-define emacs-lisp-mode 
    (:title "Emacs Lisp Mode" :color pink :separator "=" :exit t)
    ("Eval"
     (("b" eval-buffer "buffer")
      ("e" eval-defun "defun")
      ("r" eval-region "region"))
     "REPL"
     (("I" ielm "ielm"))
     "Info"
     (("D" find-function-at-point "find-function-at-point"))
     "Test"
     (("t" ert "prompt")
      ("T" (ert t) "all")
      ("F" (ert :failed) "failed"))
     "Doc"
     (("d" describe-foo-at-point "thing-at-pt")
      ("f" describe-function "function")
      ("v" describe-variable "variable")
      ("i" info-lookup-symbol "info lookup")))))

(use-package sly
  :config
  (setq inferior-lisp-program "ecl"
	sly-complete-symbol-function 'sly-simple-completions
	sly-net-coding-system 'utf-8-unix
	sly-auto-select-connection 'always
	common-lisp-hyperspec-root (expand-file-name (concat user-emacs-directory "etc/HyperSpec/"))

	sly-kill-without-query-p t
	sly-description-autofocus t
	sly-inhibit-pipelining nil
	sly-load-failed-fasl 'always

	lisp-loop-indent-subclauses nil
	lisp-loop-indent-forms-like-keywords t
	lisp-lambda-list-keyword-alignment t
	sly-export-symbol-representation-auto t
	common-lisp-style 'modern
	sly-export-save-file t)
  (defun fer/sly-hyper-lookup ()
    (interactive)
    (eww-open-file (concat common-lisp-hyperspec-root "Body/"
			   (common-lisp-hyperspec--strip-cl-package
			    (car (common-lisp-hyperspec--find (common-lisp-hyperspec-read-symbol-name)))))))

  (setq sly-documentation-lookup-function 'fer/sly-hyper-lookup)

  (defun fer/sly-header ()
    "Custom sly repl header."
    "
		    .,@@@@@@@@@@@@@@# *               
	       ,        ,@@@@@@@@@@@@@@@@(,           
			    .@@@@@@@@@@@@@@@%.        
			       @@@@@@@@@@@@@@@@,      
				@@@   &@@@@@@@@@&     
	  @@@@         @@@&     %@@@    @@@@@@@@@@/   
	   @@@.      @@@@.      #@@@@    @@@@@@@@@@,  
      /    #@@@     @@@@        @@@@@     @@@@@@@@@*  
	    @@@@   @@@@        @@@@@%      %@@@@@@@@  
	     @@@@ @@@@       @@@@@@@    @   %@@@@@@@  
	      @@@@@@@      @@@@@@@@    @@@   &@@@@@@. 
	       &@@@@@     @@@@@@@&   *@@@@&   @@@@@,  
       .        /@@@@    #@@@@@@*   @@@@@@@(   @@@@.  
	.         @@@@   %@@@@@    @@@@@@@@@    @@*   
	 /         @@@@   @@@@@@@@@@@@@@@@@@@@@@.     
	   *               @@@@@@@@@@@@@@@@@@@%(      
	     *               @@@@@@@@@@@@@@@          
		.               /@@@@@@@&             
				    ..                

    Happy hacking!! ")
  (advice-add 'sly-mrepl-random-sylvester :override #'fer/sly-header))


  ;; (use-package cl-erefactor
  ;;   :straight (cl-erefactor :type git :host gitlab :repo "sasanidas/cl-erefactor.el"
  ;; 			  :files ("cl-erefactor.el"
  ;; 				  "semantic/cl-erefactor-semantic.el")))

  (use-package clede)



  (use-package sly-mrepl
    :after sly
    :config
    (setq sly-mrepl-history-file-name (expand-file-name (concat user-emacs-directory "var/.sly-repl-history")))
    (define-key sly-mrepl-mode-map (kbd "C-q") 'isearch-backward))

  ;; (use-package sly-quicklisp
  ;;   :straight t
  ;;   :after sly
  ;;   :defer t)

  (use-package helm-sly
    :after sly)


  (with-eval-after-load 'major-mode-hydra
    (major-mode-hydra-define lisp-mode
      (:title "Common Lisp Mode" :color red :separator "/" :exit t)
      ("Eval"
       (("b" sly-eval-buffer "buffer")
	("e" sly-eval-defun "defun")
	("r" sly-eval-region "region"))
       "REPL"
       (("q"  sly-mrepl "go")
	("R"  sly-restart-inferior-lisp "restart")
	("S"  sly-mrepl-set-package "set package"))
       "Info"
       (("." xref-find-definitions  "find-function-at-point")
	("h" helm-etags "tags")
	("c" sly-who-calls "function calls")
	("v" sly-who-references "variable references")
	("V" sly-who-sets "variable sets")
	("I" sly-inspect "inspect variable"))
       "Debug"
       (("t" sly-toggle-fancy-trace "toogle ftrace"))
       "Doc"
       (("d" sly-documentation-lookup "thing-at-pt")
	("a" helm-sly-apropos "apropos")
	("f" sly-describe-function "function")
	("s" sly-describe-symbol "symbol")
	("i" sly-documentation "info lookup"))
       "Misc"
       (("g" (fer/generate-tags 'lisp-mode) "generate tags")
	("-" (fer/indent-buffer) "indent buffer")))))

; (add-to-list 'load-path "/home/fermin/Programming/ecb-2.40/")

)
