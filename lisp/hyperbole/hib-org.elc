;ELC   
;;; Compiled
;;; in Emacs version 27.2
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\301\302!\210\303\304\"\207" [symtable:ibtypes require hsys-org symtable:add org-mode] 3)
#@1148 Follow Org mode references, cycles outline visibility and executes code blocks.

First, this follows internal links in Org mode files.  When pressed on a
link referent/target, the link definition is displayed, allowing two-way
navigation between definitions and targets.

Second, this follows Org mode external links.

Third, within a radio target definition, this jumps to the first
occurrence of an associated radio target.

Fourth, when point is on an outline heading in Org mode, this
cycles the view of the subtree at point.

Fifth, with point on the first line of a code block definition, this
executes the code block via the Org mode standard binding of {C-c C-c},
(org-ctrl-c-ctrl-c).

In any other context besides the end of a line, the Action Key invokes the
Org mode standard binding of {M-RET}, (org-meta-return).

To disable ALL Hyperbole support within Org major and minor modes, set the
custom option `inhibit-hsys-org' to t.  Then in Org modes, this will
simply invoke `org-meta-return'.

Org links may be used outside of Org mode buffers.  Such links are
handled by the separate implicit button type, `org-link-outside-org-mode'.
(defalias 'ibtypes::org-mode #[nil "\305	 \205q \306\307!?\205q \n\203 \310!\202q \311 \203, \312\313 \314 B!\210\315\f\"\202q \316 \211\203= \312!\210\317!\202q \320 \203H \321!\202q \322 \211\203Y \312!\210\323!\202q \324 \203d \325!\202q \326 \203n \327 \202q \310!)\207" [start-end hsys-org-mode-function inhibit-hsys-org hrule:action current-prefix-arg nil hyperb:stack-frame (org-meta-return) org-meta-return hsys-org-agenda-item-at-p hsys-org-set-ibut-label line-beginning-position line-end-position org-agenda-show-and-scroll-up hsys-org-internal-link-target-at-p org-internal-link-target hsys-org-radio-target-def-at-p org-radio-target hsys-org-link-at-p org-link org-at-heading-p hsys-org-cycle hsys-org-block-start-at-p org-ctrl-c-ctrl-c] 3 (#$ . 513)])
(byte-code "\300\301\302\"\210\303\304\305\306#\210\307\310!\207" [setplist ibtypes::org-mode (definition-name org-mode quote (to-p nil style nil)) symset:add org-mode ibtypes symbols run-hooks htype-create-hook] 4)
#@136 If on an Org mode heading, cycles through views of the whole buffer outline.
If on an Org mode link, displays standard Hyperbole help.
(defalias 'org-mode:help #[(&optional _but) "\303 \204\n \304 \203 \305!\210\306\207	 \205 \307 \205 \n\310!\210\306\207" [current-prefix-arg hsys-org-mode-function hrule:action hsys-org-link-at-p hsys-org-agenda-item-at-p hkey-help t org-at-heading-p hsys-org-global-cycle] 2 (#$ . 2661)])
(symtable:add 'org-link symtable:actypes)
#@124 Follows an optional Org mode LINK to its target.
If LINK is nil, follows any link at point.  Otherwise, triggers an error.
(defalias 'actypes::org-link #[(&optional link) ";\203 \301\302!\203 \302!\207\301\303!\205 \303!\207\304 \207" [link fboundp org-link-open-from-string org-open-link-from-string org-open-at-point] 2 (#$ . 3140)])
(byte-code "\301\302\303\"\210\304\305\306\307#\210\310\311!\210\312\313\"\207" [symtable:actypes setplist actypes::org-link (definition-name org-link) symset:add org-link actypes symbols run-hooks htype-create-hook symtable:add org-internal-link-target] 4)
#@159 Follows an optional Org mode LINK-TARGET back to its link definition.
If LINK-TARGET is nil, follows any link target at point.  Otherwise, triggers an error.
(defalias 'actypes::org-internal-link-target #[(&optional link-target) "\302	;\203 \303\304	!\210\202$ 	\204$ \305 \211\203$ \304\306@A\"!\210?\205, \307\310!)\207" [start-end link-target nil t hsys-org-search-internal-link-p hsys-org-internal-link-target-at-p buffer-substring-no-properties error "(org-internal-link-target): Point must be on a link target (not the link itself)"] 4 (#$ . 3748)])
(byte-code "\301\302\303\"\210\304\305\306\307#\210\310\311!\210\312\313\"\207" [symtable:actypes setplist actypes::org-internal-link-target (definition-name org-internal-link-target) symset:add org-internal-link-target actypes symbols run-hooks htype-create-hook symtable:add org-radio-target] 4)
#@189 Jump to the next occurrence of an optional Org mode radio TARGET link.
If TARGET is nil and point is on a radio target definition or link, it
uses that one.  Otherwise, triggers an error.
(defalias 'actypes::org-radio-target #[(&optional target) "\302	;\203 \303\304	!\210\202$ 	\204$ \305 \211\203$ \304\306@A\"!\210?\205, \307\310!)\207" [start-end target nil t hsys-org-to-next-radio-target-link hsys-org-radio-target-at-p buffer-substring-no-properties error "(org-radio-target): Point must be on a radio target definition or link"] 4 (#$ . 4617)])
(byte-code "\300\301\302\"\210\303\304\305\306#\210\307\310!\210\311\312!\207" [setplist actypes::org-radio-target (definition-name org-radio-target) symset:add org-radio-target actypes symbols run-hooks htype-create-hook provide hib-org] 4)
